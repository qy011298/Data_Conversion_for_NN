filepath = "adult_valid.txt"
areaMappings = {
     "Private": 1,
     "Public": 2,
     "Without-pay":3, 
     "Never-worked":4,
     "Self-emp-not-inc":5,
     "State-gov":6,
     "Federal-gov":7,
     "Local-gov":8,
     "Self-emp-inc":9,
     "?":0
}
educationMappings = {
    "11th":1,
    "HS-grad":2,
    "Assoc-acdm":3,
    "Some-college":4,
    "10th":5,
    "Prof-school":6,
    "7th-8th":7,
    "Bachelors":8,
    "Masters":9,
    "Doctorate":10,
    "5th-6th":11,
    "Assoc-voc":12,
    "9th":13,
    "12th":14,
    "1st-4th":15,
    "Preschool":16
}
relaStatMappings = {
        "Never-married":1,
        "Married-civ-spouse":2,
        "Widowed":3,
        "Divorced":4,
        "Separated":5,
        "Married-spouse-absent":6,
        "Married-AF-spouse":7
}
industryMappings = {
    "Machine-op-inspct":1,
    "Farming-fishing":2,
    "Protective-serv":3,
    "?":4,
    "Other-service":5,
    "Prof-specialty":6,
    "Craft-repair":7,
    "Adm-clerical":8,
    "Exec-managerial":9,
    "Tech-support":10,
    "Sales":11,
    "Priv-house-serv":12,
    "Transport-moving":13,
    "Handlers-cleaners":14,
    "Armed-Forces":15
}
raceMappings = {
    "Black":1,
    "White":2,
    "Asian-Pac-Islander":3,
    "Other":4,
    "Amer-Indian-Eskimo":5
}
sexMappings = {
    "Male":1,
    "Female":2
}
outputMappings = {
    "<=50K.\n":0,
    ">50K.\n":1,
    "<=50K\n":0,
    ">50K\n":1
}

uniqueEntries = {}
# list of all rows (tuples) here
allRows = list()



with open(filepath) as fp:
    line = fp.readline()
    while line:
        splitted = line.split(', ')
        newstr = splitted[0].replace("'","");
        newTuple = (newstr,areaMappings[splitted[1]], educationMappings[splitted[3]], relaStatMappings[splitted[5]], industryMappings[splitted[6]],raceMappings[splitted[8]], sexMappings[splitted[9]],outputMappings[splitted[14]])
        uniqueEntries[splitted[9]] = 1
        line = fp.readline()
        

        # add to list above
        allRows.append(newTuple)


# you now have a list of converted rows
# now iterate over list & write out 

for item in allRows:
    for value in item:
        print(str(value), end="\t")
    print("\r")
